## URL Analyser

### Approach
* The URL Analyser is designed to parse log files, extract attributes from each log line, and generate metrics based on the log data. Each log line contains multiple components, which can be extracted using a regular expression. For example, given the following log line: 

```
177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"
```
Contains following attributes 

* `177.71.128.21` - ip address  
* `[10/Jul/2018:22:21:28 +0200]` - Timestamp 
* `GET` - Http method
* `/intranet-analytics/` - URL
* `200` - status code
* `"-"` - Referrer 
* `"Mozilla/5.0 ..... 6 Safari/534.7"` - User Agent 

After destructing common components of each log line, a [named capturing group](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Regular_expressions/Named_capturing_group) expression is constructed to identify key attributes of each log line.

Once key atrributes are identified then using filtering techniques required metrics are extracted.

Assumptions
* Log lines follow consistant patten 
* Users will provide log file input as file paths 

### How can I run it?

#### Firstly, install required npm packages :

(From root Dir)

`npm i`

#### Then run the application with 

(From root Dir)

`node .\index.js <Input file path>`