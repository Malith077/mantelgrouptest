import analyseLog from "./modules/analyser.js";

/**
 * Main function that reads the log file path from the command line arguments and analyses the log file
 * 
 */

const main = async () => {
    try{
        const args = process.argv.slice(2);
        if (args.length < 1) {
            console.log('Usage: node index.js <log_file_path>');
            process.exit(1);
        }
    
        const logFilePath = args[0];
        
        const results = await analyseLog(logFilePath);

        if (!results) {
            process.exit(1);
        }

        const { uniqueIps, mostVisitedUrls, mostActiveIPs } = results;
        console.log(`Number of unique IP addresses: ${uniqueIps}`);
        console.log(`Top 3 most visited URLs: ${mostVisitedUrls}`);
        console.log(`Top 3 most active IP addresses: ${mostActiveIPs}`);

    } catch (error) { 
        console.error(`Error: ${error.message}`);
    }
};


main();