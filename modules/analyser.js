import fileReader from "./fileReader.js";
import parseLogLine from "./parser.js";

/**
 * A function that analyses a log file by counting unique IPs, most visited URLs and most active IPs
 * 
 * @param {filePath} filePath as a string
 * @returns Object with the following properties:
 * - uniqueIps: Number of unique IPs
 * - mostVisitedUrls: Array of top 3 most visited URLs
 * - mostActiveIPs: Array of top 3 most active IPs
 */

const analyseLog = async (filePath) => {
    const readLines = await fileReader(filePath);

    if (!readLines) {
        return;
    }

    const ipCount = {};
    const urlCount = {};
    for ( const line of readLines) {
        const parsedLine = parseLogLine(line);
        if (!parsedLine) {
            continue;
        }
        const { ip, url } = parsedLine;
        ipCount[ip] = ipCount[ip] ? ipCount[ip] + 1 : 1;
        urlCount[url] = urlCount[url] ? urlCount[url] + 1 : 1;
    }

    // Get Unique IPs
    const uniqueIps = Object.keys(ipCount).length;
    // Get Most Visited URLs - descending order
    const mostVisitedUrls = Object.keys(urlCount).sort((a, b) => urlCount[b] - urlCount[a]).slice(0, 3);
    // Get Most Active IPs - descending order
    const mostActiveIPs = Object.keys(ipCount).sort((a, b) => ipCount[b] - ipCount[a]).slice(0, 3);

    return { uniqueIps, mostVisitedUrls, mostActiveIPs };
}

export default analyseLog;