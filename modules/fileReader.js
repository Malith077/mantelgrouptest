import fs from 'fs';

/**
 * A function that reads a file and splits its content into an array of lines
 * 
 * @param {filePath} filePath to be read
 * @returns contest of the file as an array of lines or null if an error occurs
 */

const fileReader = async (filePath) => {
    try {
        const data =  fs.readFileSync(filePath, 'utf8');
        const lines = data.split('\n');
        return lines;
    } catch (error) {
        console.error(`Error reading file: ${filePath}`);
        return null;
    }
}

export default fileReader;