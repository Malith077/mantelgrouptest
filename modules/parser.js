/**
 * A function that parses a single line of a log file
 * 
 * @param {line} line to be parsed
 * @returns Matched groups from the line or null if no match
 */

const parseLogLine = (line) => {
    const regex = /^(?<ip>\d+\.\d+\.\d+\.\d+) - - \[(?<datetime>[^\]]+)\] "(?<method>[A-Z]+) (?<url>[^ ]+) (?<protocol>[^"]+)" (?<status>\d+) (?<size>\d+) "(?<referrer>[^"]*)" "(?<userAgent>[^"]*)"/;
    const match = line.match(regex);
    if (!match) {
        return null;
    }
    return match.groups;
}

export default parseLogLine;