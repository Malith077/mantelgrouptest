import { expect } from 'chai';
import fileReader from "../modules/fileReader.js";

import fs from 'fs';
import sinon from 'sinon';

describe('File Reader', () => {

    afterEach(() => {
        if (fs.existsSync('test/test_log.txt')) {
            fs.unlinkSync('test/test_log.txt');
        }
    })

    it('should read and split the file content correctly', async () => {
        const fileContent = [
            '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"',
            '168.41.191.40 - - [09/Jul/2018:10:11:30 +0200] "GET http://example.net/faq/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"',
        ].join('\n');

        fs.writeFileSync('test/test_log.txt', fileContent);

        const lines = await fileReader('test/test_log.txt');
        expect(lines).to.deep.equal([
            '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"',
            '168.41.191.40 - - [09/Jul/2018:10:11:30 +0200] "GET http://example.net/faq/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"',
        ]);
    })

    it('should return null and error on non existent file', async () => {
        const consoleSpy = sinon.spy(console, 'error');
        const lines = await fileReader('test/mysterious_file.txt');

        expect(lines).to.be.null;
        expect(consoleSpy.calledOnce).to.be.true;
        expect(consoleSpy.calledWith('Error reading file: test/mysterious_file.txt')).to.be.true;
    });

    it('should return an empty file content', async () =>{
        fs.writeFileSync('test/test_log.txt', '');

        const lines = await fileReader('test/test_log.txt');
        expect(lines).to.deep.equal(['']);
    })
})