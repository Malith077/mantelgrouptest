import { expect  } from "chai";

import parseLogLine from "../modules/parser.js";

describe("Parser Analysis", () => {
    it('should parse log line correctly' , () => {
        const logLine = '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"';
        const expected = {
            ip: '177.71.128.21',
            datetime: '10/Jul/2018:22:21:28 +0200',
            method: 'GET',
            url: '/intranet-analytics/',
            protocol: 'HTTP/1.1',
            status: '200',
            size: '3574',
            referrer: '-',
            userAgent: 'Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7'
        };
        expect(parseLogLine(logLine)).to.deep.equal(expected);
    });

    it('should return null for an invalid log line', () => {
        const logLine = 'Invalid log line';
        expect(parseLogLine(logLine)).to.be.null;
    });

    it('should return null for a log line with missing fields', () => {
        const logLine = '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574';
        const parsedLine = parseLogLine(logLine);
        expect(parsedLine).to.be.null;
    });
});