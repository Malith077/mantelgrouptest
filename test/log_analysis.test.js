import { expect } from "chai";
import parseLogLine from "../modules/parser.js";
import analyseLog from "../modules/analyser.js";
import fileReader from "../modules/fileReader.js";

import fs from "fs";

describe( "Log Analysis" , () => {

    afterEach(() => {
        if (fs.existsSync('test/test_log.txt')) {
            fs.unlinkSync('test/test_log.txt');
        }
    })

    it('should analyze the log correctly', async () => {
        const logContent = [
            '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"',
            '168.41.191.40 - - [09/Jul/2018:10:11:30 +0200] "GET http://example.net/faq/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"',
            '177.71.128.21 - - [10/Jul/2018:22:22:08 +0200] "GET /blog/2018/08/survey-your-opinion-matters/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6"',
        ];
        fs.writeFileSync('test/test_log.txt', logContent.join('\n'));

        const result = await analyseLog('test/test_log.txt');
        const { uniqueIps, mostVisitedUrls, mostActiveIPs } = result;

        expect(uniqueIps).to.equal(2);
        expect(mostVisitedUrls).to.deep.equal([
            '/intranet-analytics/',
            'http://example.net/faq/',
            '/blog/2018/08/survey-your-opinion-matters/'
        ]);
        expect(mostActiveIPs).to.deep.equal( [ '177.71.128.21', '168.41.191.40' ] );
    });

    it('should handle an empty log file correctly', async () => {
        const logContent = [];
        fs.writeFileSync('test/test_log.txt', logContent.join('\n'));

        const result = await analyseLog('test/test_log.txt');
        const { uniqueIps, mostVisitedUrls, mostActiveIPs } = result;

        expect(uniqueIps).to.equal(0);
        expect(mostVisitedUrls).to.deep.equal([]);
        expect(mostActiveIPs).to.deep.equal([]);    
    });

    it('should handle a log file with invalid lines correctly', async () => {
        const logContent = [
            '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"',
            'Invalid log line',
            'Another invalid log line',
        ];
        fs.writeFileSync('test/test_log.txt', logContent.join('\n'));
        
        const result = await analyseLog('test/test_log.txt');
        const { uniqueIps, mostVisitedUrls, mostActiveIPs } = result;
        expect(uniqueIps).to.equal(1);
        expect(mostVisitedUrls).to.deep.equal(['/intranet-analytics/']);
        expect(mostActiveIPs).to.deep.equal(['177.71.128.21']);
    });

    it('should handle with duplicate log lines correctly', async () => {
        const logContent = [
            '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"',
            '168.41.191.40 - - [09/Jul/2018:10:11:30 +0200] "GET http://example.net/faq/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"',
            '177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] "GET /intranet-analytics/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7"',
            '168.41.191.40 - - [09/Jul/2018:10:11:30 +0200] "GET http://example.net/faq/ HTTP/1.1" 200 3574 "-" "Mozilla/5.0 (Linux; U; Android 2.3.5; en-us; HTC Vision Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"',
        ];

        fs.writeFileSync('test/test_log.txt', logContent.join('\n'));

        const result = await analyseLog('test/test_log.txt');
        const { uniqueIps, mostVisitedUrls, mostActiveIPs } = result;

        expect(uniqueIps).to.equal(2);
        expect(mostVisitedUrls).to.deep.equal([
            '/intranet-analytics/',
            'http://example.net/faq/',
        ]);
        expect(mostActiveIPs).to.deep.equal(['177.71.128.21', '168.41.191.40']);
            
    });

    
});